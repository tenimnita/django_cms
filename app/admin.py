# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import slugify
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from dal import autocomplete
from django import forms
from django.contrib import admin

# Register your models here.

from django.contrib import admin
from django.utils.encoding import force_text

from app.models import Tag
from .models import Category, Subcriber, Article
from .common.logger import log


class TagAutocomplete(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        # Don't forget to filter out results depending on the visitor !
        if not self.request.user.is_authenticated():
            return Tag.objects.none()

        qs = Tag.objects.all()

        if self.q:
            qs = qs.filter(tag_name__istartswith=self.q)

        return qs


class ArticleForm(forms.ModelForm):
    content = forms.CharField(widget=CKEditorUploadingWidget())
    tags = forms.ModelMultipleChoiceField(queryset=Tag.objects.all(),
                                          widget=autocomplete.ModelSelect2Multiple(
                                              url='tag-autocomplete',
                                              forward=("tags",)))

    class Meta:
        model = Article
        fields = ('is_highlight', 'image', 'category', 'title', 'intro', 'content', 'tags', 'create_date')

    def is_valid(self):
        log.info(force_text(self.errors))
        return super(ArticleForm, self).is_valid()


class ArticleAdmin(admin.ModelAdmin):
    form = ArticleForm

    list_display = ('id', 'category', 'title')
    list_display_links = ('id', 'category', 'title')

    # def save_model(self, request, obj, form, change):
    #     print request, obj, form, change
    #     super(ArticleAdmin, self).save_model(request, obj, form, change)


admin.site.register(Category)
admin.site.register(Subcriber)
admin.site.register(Tag)
admin.site.register(Article, ArticleAdmin)
