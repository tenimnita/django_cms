from django import forms
from django.http.response import JsonResponse, HttpResponseBadRequest
from django.views.decorators.http import require_POST, require_http_methods

from app.models import Subcriber


def _success(x):
    return JsonResponse({
        'status': 'success',
        'payload': x
    })


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


class SubscribeForm(forms.Form):
    email = forms.CharField(min_length=3, max_length=100)


@require_http_methods(["POST"])
def subscribe(request):
    form = SubscribeForm(request.POST)
    if not form.is_valid():
        return HttpResponseBadRequest()
    email = request.POST["email"]
    if Subcriber.objects.filter(email=email).count() > 0:
        return _success({})
    ip = get_client_ip(request)
    Subcriber.objects.create(
        email=email,
        ip_address=ip,
        name='',
    )
    return _success({})
