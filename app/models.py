# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from ckeditor.fields import RichTextField
from django.db import models

# Create your models here.
from django.utils import timezone

from django_cms import settings
from slugify import slugify

class Category(models.Model):
    class Meta:
        db_table = settings.DB_TABLE_PREFIX + '_category'
        verbose_name = 'Category'

    category_name = models.CharField(max_length=200)

    def __str__(self):
        return self.category_name

class Tag(models.Model):
    class Meta:
        db_table = settings.DB_TABLE_PREFIX + '_tag'
        verbose_name = 'Tag'

    tag_name = models.CharField(max_length=200, unique=True)
    create_time = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.tag_name


class Article(models.Model):
    class Meta:
        db_table = settings.DB_TABLE_PREFIX + '_article'
        verbose_name = 'Article'

    is_highlight = models.BooleanField(default=False)
    image = models.ImageField(upload_to='images/', null=True)
    category = models.ForeignKey(Category, null=True, db_constraint=False)
    title = models.CharField(max_length=100)
    intro = models.CharField(max_length=2000)
    content = models.TextField()
    create_date = models.DateTimeField(default=timezone.now)
    view_count = models.IntegerField(default=0)
    slug = models.CharField(max_length=200, null=True, unique=True)
    tags = models.ManyToManyField(Tag, db_constraint=False, blank=True)

    create_time = models.DateTimeField(default=timezone.now)

    def __unicode__(self):
        return self.title

    def save(self, *args, **kwargs):
        self.slug = slugify(self.title)
        super(Article, self).save(*args, **kwargs)


class Subcriber(models.Model):
    class Meta:
        db_table = settings.DB_TABLE_PREFIX + '_subcriber'
        verbose_name = 'Subcriber'

    name = models.CharField(max_length=100)
    email = models.EmailField(max_length=1000)
    ip_address = models.CharField(max_length=100)
    captcha_score = models.FloatField(null=True)
    create_time = models.DateTimeField(default=timezone.now)

    def __str__(self):
        return self.email
