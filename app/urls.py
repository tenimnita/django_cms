from django.conf.urls import url

from app import api
from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^(?P<slug>[a-zA-Z0-9-]+)-(?P<article_id>[0-9]+)/$', views.article_detail, name='article_detail'),
    url(r'^tag/(?P<tag_name>[a-zA-Z0-9]+)/$', views.tag_detail, name='tag_detail'),

    url(r'^api/subscribe', api.subscribe),
]
