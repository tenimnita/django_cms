# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render
from slugify import slugify
# Create your views here.
from django.template import loader
from django.http import HttpResponse
from django.shortcuts import render, get_object_or_404, redirect, reverse
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from app.models import Article, Tag

ARTICLE_PER_PAGE = 1


def index(request):
    high_light_articles = Article.objects.filter(is_highlight=True).order_by('-id')[:5]
    context = {
        'high_light_articles': high_light_articles,
        'latest_article': Article.objects.latest('id'),
        'recent_articles': Article.objects.order_by('-id')[1:4]
    }
    return render(request, "app/index.html", context)


def article_detail(request, slug, article_id):
    article = get_object_or_404(Article, pk=article_id)
    print article.slug, slug
    if article.slug != slug:
        return redirect(reverse('article_detail', args=(article.slug, article_id)))
    latest_article = Article.objects.order_by('-id')[:3]
    most_view_tags = Tag.objects.order_by('-id')[:5]

    return render(request, 'app/article_detail.html', {
        'article': article,
        'latest_article': latest_article,
        'most_view_tags': most_view_tags
    })


def tag_detail(request, tag_name):
    tag = get_object_or_404(Tag, tag_name=tag_name)

    all_articles = tag.article_set.all()
    paginator = Paginator(all_articles, ARTICLE_PER_PAGE)

    page = request.GET.get('page')
    try:
        articles = paginator.page(page)
        if int(page) == 1:
            return redirect(reverse('tag_detail', args=(tag_name,)))
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        articles = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        articles = paginator.page(paginator.num_pages)
    return render(request, 'app/tag_detail.html', {
        'tag': tag,
        'articles': articles
    })


def subscribe(request, email):
    pass
