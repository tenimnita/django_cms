
DB_TABLE_PREFIX = 'cms'

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': 'django_cms',
        'USER': 'root',
        'PASSWORD': '123456a@',
        'HOST': 'localhost',
        'PORT': 3306,
        'CONN_MAX_AGE': 3600,
        'OPTIONS': {'charset': 'utf8mb4'}
    },
}
