DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.mysql',
        'NAME': '',
        'USER': '',
        'PASSWORD': '',
        'HOST': '',
        'PORT': 3306,
        'CONN_MAX_AGE': 3600,
        'OPTIONS': {'charset': 'utf8mb4'}
    },
}

MEDIA_ROOT = ''
STATIC_ROOT = ''

HOME_PAGE = ''

# if not HOME_PAGE.startswith('https://'):
#     SESSION_COOKIE_SECURE = False
#     CSRF_COOKIE_SECURE = False

DOMAIN = HOME_PAGE.split("//")[-1].split("/")[0]

ALLOWED_HOSTS = [DOMAIN, ]