import os

from django_cms.config.live import DOMAIN
from django_cms.settings import BASE_DIR, DEBUG

LOGGING_ROLLOVER_WHEN = 'midnight'
LOGGING_BACKUP_COUNT = 10

LOG_DIR = BASE_DIR

if not DEBUG:
    LOG_DIR = '/var/log/nginx/%s' % DOMAIN

LOGGING = {
    'version': 1,
    'disable_existing_loggers': True,
    'filters': {
        'require_debug_true': {
            '()': 'django.utils.log.RequireDebugTrue',
        },
        'require_debug_false': {
            '()': 'django.utils.log.RequireDebugFalse',
        },
    },
    'formatters': {
        'standard': {
            'format': '%(asctime)s.%(msecs)03d|%(levelname)s|%(process)d:%(thread)d|%(filename)s:%(lineno)d|%(module)s.%(funcName)s|%(message)s',
            'datefmt': '%Y-%m-%d %H:%M:%S',
        },
        'verbose': {
            'format': '{levelname} {asctime} {module} {process:d} {thread:d} {message}',
            'style': '{',
        },
        'simple': {
            'format': '{levelname} {message}',
            'style': '{',
        },
    },
    'handlers': {
        'console': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],
            'class': 'logging.StreamHandler',
        },
        'file_debug': {
            'level': 'DEBUG',
            'filters': ['require_debug_true'],  # do not run debug logger in production
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(LOG_DIR, 'log/debug.log'),
            'when': LOGGING_ROLLOVER_WHEN,
            'backupCount': LOGGING_BACKUP_COUNT,
            'formatter': 'standard'
        },
        'file_info': {
            'level': 'INFO',
            'filters': [],  # run logger in production
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(LOG_DIR, 'log/info.log'),
            'when': LOGGING_ROLLOVER_WHEN,
            'backupCount': LOGGING_BACKUP_COUNT,
            'formatter': 'standard'
        },
        'file_error': {
            'level': 'ERROR',
            'filters': ['require_debug_false'],  # run logger in production
            'class': 'logging.handlers.TimedRotatingFileHandler',
            'filename': os.path.join(LOG_DIR, 'log/error.log'),
            'when': LOGGING_ROLLOVER_WHEN,
            'backupCount': LOGGING_BACKUP_COUNT,
            'formatter': 'standard'
        },
    },
    'loggers': {
        'django.db.backends': {
            'level': 'DEBUG',
            'handlers': ['console', 'file_error'],
            'propagate': True,
        },
        'main': {
            'handlers': ['file_debug', 'file_info', 'file_error'],
            'level': 'DEBUG',
            'propagate': True,
        },
        # Again, default Django configuration to email unhandled exceptions
        'django.request': {
            'handlers': ['file_error', 'file_info', ],
            'level': 'ERROR',
            'propagate': True,
        },
        # Might as well log any errors anywhere else in Django
        # 'django': {
        #     'handlers': ['file_error'],
        #     'level': 'ERROR',
        #     # 'propagate': False,
        # },
    }
}