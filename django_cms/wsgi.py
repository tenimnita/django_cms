"""
WSGI config for django_cms project.

It exposes the WSGI callable as a module-level variable named ``application``.

For more information on this file, see
https://docs.djangoproject.com/en/1.11/howto/deployment/wsgi/
"""

import os
import socket

from django.core.wsgi import get_wsgi_application

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "django_cms.settings")


# Monkey Patch: Zombie Workers
# This is to prevent SQL connections not closed if non-200 status codes are returned
from gunicorn import util

def close(sock):
    from django.db import connections
    for conn in connections.all():
        try:
            conn.abort()
        except:
            pass
        conn.close()
    try:
        sock.close()
    except socket.error:
        pass

util.close = close

try:
    import pymysql
    pymysql.install_as_MySQLdb()
except ImportError:
    pass

from django.core.wsgi import get_wsgi_application

application = get_wsgi_application()
