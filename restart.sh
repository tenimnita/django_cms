#!/usr/bin/env bash
kill -9 `cat start.pid`
gunicorn -c config.ini --daemon django_cms.wsgi:application